%{
	#include <math.h>
	#include <stdio.h>
	#include <stdlib.h>
	#define YYSTYPE double
%}

%token RLIT ILIT 
%token PLUS MINUS TIMES DIVIDE REM 
%token ASSIGN
%token LP RP
%token ID
%token SEMI COMMA LEXERR
%token INT REAL

%right ASSIGN
%left PLUS MINUS
%left TIMES DIVIDE REM
%right UMINUS

%start PROG

%%

PROG:   STMTS			{printf ("PROG -> STMTS\n"); printf("ACCEPT\n");};

STMTS: 	STMT STMTS		{printf ("STMTS -> STMT STMTS\n"); $$=$2;}
	| /* empty */		{printf ("STMTS -> epsilon\n");};

STMT:   DTYPE ID IDLIST SEMI	{printf ("STMT -> DTYPE id IDLIST ;\n"); $$=$4;}
       	|ID ASSIGN EXPR SEMI	{printf("STMT -> id := EXPR ;\n"); $$=$4;}

IDLIST:	COMMA ID IDLIST		{printf("IDLIST -> , id IDLIST\n"); $$=$3;}
       	|/*empty*/		{printf ("IDLIST -> epsilon\n");};

EXPR:  	EXPR OPTR TERM 		{printf("EXPR -> EXPR OPTR TERM\n"); $$=$3;}
	|TERM 			{printf("EXPR -> TERM\n"); $$=$1;}
	|LP EXPR RP 		{printf("EXPR -> ( EXPR )\n"); $$=$3;}
	|MINUS EXPR %prec UMINUS {printf("EXPR -> uminus EXPR\n"); $$=$2;};

TERM:   ID			{printf("TERM -> id\n"); $$=$1;}
	|NUM			{printf("TERM -> num\n"); $$=$1;};

NUM:    ILIT			{$$ = $1;}
	|RLIT			{$$ = $1;}

DTYPE:  INT 			{printf("DTYPE -> int\n");  $$=$1;}
	|REAL 			{printf("DTYPE -> real\n"); $$=$1;}

OPTR: 	PLUS 			{printf("OPTR -> +\n", $$=$1);}
    	|MINUS 			{printf("OPTR -> -\n", $$=$1);}
    	|TIMES 			{printf("OPTR -> *\n", $$=$1);}
    	|DIVIDE 		{printf("OPTR -> /\n", $$=$1);}
	|REM 			{printf("OPTR -> %%\n", $$=$1);};

%%

extern int yylex(void);
extern int yyparse(void);

int yyerror(char *s) {
   printf("%s\n",s);
}	

int yywrap(void) {
    return 1;
}

int main() {
	return yyparse();  
}   