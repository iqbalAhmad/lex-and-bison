As for today (2011-04-05) you will need the lastest versions of:

flex-2.5.4a-1.exe
bison-2.4.1-setup.exe

After that, do a full install in a directory of your preference without spaces in the name. I suggest C:\GnuWin32. 
Do not install it in the default (C:\Program Files (x86)\GnuWin32) because bison has problems with spaces in directory names, not to say parenthesis.

Also, consider installing Dev-CPP in the default directory (C:\Dev-Cpp)

After that, set the PATH variable to include the bin directories of gcc (in C:\Dev-Cpp\bin) and flex\bison (in C:\GnuWin32\bin). 
To do that, copy this:  ;C:\Dev-Cpp\bin;C:\GnuWin32\bin and append it to the end of the PATH variable, defined in the place show by this figure:
step-by-step to set PATH variable under Win-7.

== After doing assignment == 
If the figure is not in good resolution, you can see a step-by-step here.
Open a prompt, cd to the directory where your ".l" and ".y" are, and compile them with:
flex hello.l
bison -dy hello.y
gcc lex.yy.c y.tab.c -o hello.exe

Commands to create lexical analyzer, parser and executable.

You will be able to run the program. I made the sources for a simple test (the infamous Hello World):

Hello.l

%{

#include <stdlib.h>
#include "y.tab.h"

%}

%%

("hi"|"oi")"\n"       { return HI;  }
("tchau"|"bye")"\n"   { return BYE; }
.                     { yyerror();  }

%%

int main(void)
{
   yyparse();
   return 0;
}

int yywrap(void)
{
   return 0;
}

int yyerror(void)
{
    printf("Error\n");
    exit(1);
}


Hello.y

%token HI BYE

%%

program: 
         hi bye
        ;

hi:     
        HI     { printf("Hello World\n");   }
        ;
bye:    
        BYE    { printf("Bye World\n"); exit(0); }