%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>

	#include "y.tab.h"		
	#define YYSTYPE double
%}



white     [ \t\r]+
digit     [0-9]
real 	  {digit}+("."{digit}+)
letter    [A-Za-z]  


%%
{white}		{}
"+" 		{printf("< PLUS >"); return PLUS;}
"-" 		{printf("< MINUS >"); return MINUS;}
"*" 		{printf("< MULTI >"); return TIMES;}
"/" 		{printf("< DIVIDE >"); return DIVIDE;}
"%" 		{printf("< REMAINDER >"); return REM;}
":="		{printf("< ASSIGN >"); return ASSIGN;}

"(" 		{printf("<LP>"); return LP;}
")" 		{printf("<RP>"); return RP;}

"\n" 		{printf("<ENDL>");}
";"		{printf("<SEMI>"); return SEMI;}
"," 		{printf("<COMMA>"); return COMMA;}

"int" 		{printf("<INT>"); return INT;}
"real"	 	{printf("<REAL>"); return REAL;}


{digit}+	{
				yylval= atoi(yytext);
				printf("<%s,ILIT>", yytext); 
				return ILIT;
			}

{real}		{	        yylval=atof(yytext);
				printf("<%s,RLIT>", yytext);
				return RLIT;
			}



{letter}({letter}|{digit})* {
				printf("<%s,ID>", yytext); 
				return ID;
			}


.			{
				printf("<INVALID SYMBOL>"); 
				return LEXERR;
			}

%%
int test(void)
{  
   return 0;
}



